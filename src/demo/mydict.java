package demo;

import org.jsoup.Jsoup;

import javax.swing.*;

public class mydict {
    public static void main(String[] args) throws Exception {
        while (true) {
            //1 输入词汇
            var w = JOptionPane.showInputDialog("请输入词汇：");
            if (w==null || w.trim().length() ==0){break;}
            //2 组合网址
            var u = "https://cn.bing.com/dict/search?q=" + w;
            //3.jsoup组件获得结果
            var ss = Jsoup.connect(u).get().select("span[class=def b_regtxt]").eachText();
            var su = new StringBuilder();
            var i=0;
            for (String s : ss) {
                su.append(String.format("%02d",++i)).append(s).append("\r\n");
            }
            //4 输出结果
            JOptionPane.showMessageDialog(null, su);
        }
        System.out.println("词典软件退出");
        System.exit(0);
    }
}
